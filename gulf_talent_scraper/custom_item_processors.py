
from scrapy.utils.markup import remove_tags


def join_and_remove_html_tags(self, unicodes):
    return remove_tags(''.join(unicodes))
