# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JobItem(scrapy.Item):
    title = scrapy.Field()
    company = scrapy.Field()
    location = scrapy.Field()
    date_added = scrapy.Field()
    path = scrapy.Field()
    role = scrapy.Field()
    requirements = scrapy.Field()
    about_the_company = scrapy.Field()

    export_column_names_ordered = ['title', 'company', 'location',
                                   'date_added', 'path', 'role', 'requirements', 'about_the_company']
