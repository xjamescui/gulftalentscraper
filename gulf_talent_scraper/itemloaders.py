from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join, MapCompose
from custom_item_processors import join_and_remove_html_tags


class JobItemLoader(ItemLoader):
    default_input_processor = MapCompose(unicode.strip)
    default_output_processor = Join()

    role_out = join_and_remove_html_tags
    requirements_out = join_and_remove_html_tags
    about_the_company_out = join_and_remove_html_tags