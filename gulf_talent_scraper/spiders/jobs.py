# -*- coding: utf-8 -*-
import scrapy
from gulf_talent_scraper.itemloaders import JobItemLoader
from gulf_talent_scraper.items import JobItem


class JobsSpider(scrapy.Spider):
    """
    Scrapes all jobs information (currently only in Saudi Arabia)
    """

    name = 'jobs'
    allowed_domains = ['gulftalent.com']
    start_urls = ['https://www.gulftalent.com/saudi-arabia/jobs']

    def parse(self, response):
        # capture selectors of all items on this page
        all_jobs_selectors = response.xpath(
            "//table[contains(@class, 'table table-section')]/tbody/tr")

        for job_item_selector in all_jobs_selectors:
            # parse each item in the list of
            yield self.parse_job(selector=job_item_selector, response=response)

        # Find the href embedded in the 'Next' page button, go there if a link exists
        next_page = response.xpath('//a[text()="Next"]/@href').extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

    def parse_job(self, selector=None, response=None):
        """
        Parse information about a job. Some information will require us hitting the job's detail page to get them.
        """

        job_detail_href = selector.xpath('td[1]/a/@href').extract_first()

        job_loader = JobItemLoader(JobItem(), selector=selector)
        job_loader.add_xpath('title', 'td[1]/a/strong/text()')
        job_loader.add_xpath('company', 'td[1]/text()')
        job_loader.add_xpath('location', 'td[2]/span/text()')
        job_loader.add_xpath('date_added', 'td[3]/text()')
        job_loader.add_value('path', job_detail_href)

        if job_detail_href is not None:
            # follow into details page to get remaining information about this job
            return response.follow(job_detail_href, callback=self.parse_job_detail, meta={'job_loader': job_loader})

        return job_loader.load_item()

    def parse_job_detail(self, response):
        """
        Parse remaining information that can only be found in the jobs detail page.
        """

        job_loader = response.meta['job_loader']
        if not job_loader:
            return None

        # update root selector to the response of the new page we are now in
        job_loader.selector = response

        # continue to populate remaining fields in the given item loader
        job_loader.add_xpath(
            'role', '//*/h3[contains(text(), "Job description")]/following-sibling::p[1][normalize-space()]')

        job_loader.add_xpath(
            'requirements', '//*/h4[contains(text(), "Requirements")]/following-sibling::p[normalize-space()]')

        job_loader.add_xpath(
            'about_the_company', '//*/h4[contains(text(), "About the Company")]/following-sibling::p[normalize-space()]')

        return job_loader.load_item()
