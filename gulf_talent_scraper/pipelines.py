# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from gulf_talent_scraper.spiders import JobsSpider
from gulf_talent_scraper.items import JobItem
from scrapy.exporters import CsvItemExporter


class CsvExportPipeline(object):

    def __init__(self):
        self.files = {}
        self.exporters = {}

    def open_spider(self, spider):
        self._create_export_directory_if_needed()

        file_ = open(self._create_export_filename_for(spider), 'wb')
        self.files[spider] = file_
        self.exporters[spider] = CsvItemExporter(file_)
        self.exporters[spider].fields_to_export = self._column_names_for_spider(spider)
        self.exporters[spider].start_exporting()

    def close_spider(self, spider):
        self.exporters[spider].finish_exporting()
        file_ = self.files.pop(spider)
        file_.close()

    def process_item(self, item, spider):
        self.exporters[spider].export_item(item)
        return item

    def _create_export_filename_for(self, spider):
        import datetime
        time_str = datetime.datetime.now().strftime('%Y%m%d')
        return './csv/%s_%s.csv' % (spider.name, time_str)

    def _column_names_for_spider(self, spider):
        """
        Fetch the column names to use in the export file
        """
        if spider.name == JobsSpider.name:
            return JobItem.export_column_names_ordered

        return []

    def _create_export_directory_if_needed(self, path='./csv'):
        import os
        if not os.path.exists(path):
            os.makedirs(path)
