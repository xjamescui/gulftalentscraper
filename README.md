This scraper will fetch all jobs in Saudi Arabia on gulftalent.com.

It is tested on python 2.7.

It is built using the Scrapy(https://scrapy.org) library.

## Installation:

1. create a virtual environment in this directory using `virtualenv`

    ** action: `virtualenv gulf_talent_env`**

    > note: `brew install virtualenv` to get virtualenv if you don't have it yet.

2. activate the virtual environment

    ** action: `source gulf_talent_env/bin/activate` **

    > note: type command `deactivate` to deactivate environment **

3. install the packages needed by this scraper. Having this virtual environment will keep packages installed here and not dilute other local directories on the running machine

    ** action: `pip install -r requirements.txt` **
    

4. Run the crawler and watch it work

    ** action: `scrapy crawl jobs` **

    > note: `job` is the name of the spider we are running

5. final output, a .csv file, will be located inside the ./csv folder: it is titled `jobs_yyyymmdd.csv`

---------------------------------------------------------------------------------------------------------------------------------------------

### More notes:

In settings.py, I have set `DOWNLOAD_DELAY` = 3 (0 by default) in order to create a delay between each url-hitting/downloading action. 
Scrapy uses a random delay interval between 0.5 * `DOWNLOAD_DELAY` and 1.5 * `DOWNLOAD_DELAY`. The unit is in seconds.

I am doing this to respect the server we are scraping from, and not be an aggressive
scraper where we hit many pages on their site in rapid successions. 

Given `DOWNLOAD_DELAY` = 3 it took me 30 minutes to scrape all 500ish items.
Feel free to change the delay, and even set it back to 0 if you want to. I am probably just being very prudent and not wanting to get banned.

For more information: https://scrapy.org